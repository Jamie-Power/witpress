package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Accounts extends Controller
{
  public static void signup()
  {
    render();
  }

  public static void logout()
  {
  session.clear();
    index();
  }

  public static void index()
  {
    render();
  }

  public static void register(User user)
  {
    Logger.info("registering " + user.firstName + " " + user.lastName + " " + user.email + " " + user.password);
    user.save();
    index();
  }

  public static void authenticate(String email, String password)
  {
    User user = User.findByEmail(email);
    if (user == null || !user.checkPassword(password))
    {
      Logger.info("Authentication failure");
      index();
    }
    session.put("logged_in_userid", user.id);
    Logger.info("Authentication success: " +  user.firstName + " " + user.lastName);
    Blog.index();
  }

  public static User getLoggedInUser()
  {
    User user = null;
    if (session.contains("logged_in_userid"))
    {
      String userId = session.get("logged_in_userid");
      user = User.findById(Long.parseLong(userId));
    }
    else
    {
      index(); // => log in
    }
    return user;
  }
}